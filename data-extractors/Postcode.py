# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 00:57:49 2016

@author: Anshit
"""


from bs4 import BeautifulSoup
import urllib
import MySQLdb as mdb
import re

con = mdb.connect('localhost', 'root', '', 'postcode')
cursor = con.cursor()

remove_if_exist = ['DROP TABLE IF EXISTS PostcodeInformation']
create_tables = ['CREATE TABLE PostcodeInformation (postcode INT(8) PRIMARY KEY,title VARCHAR(200) NOT NULL , area_2 VARCHAR(50) , area_1 VARCHAR(50) , city VARCHAR(20) NOT NULL , region_3 VARCHAR(50) , region_2 VARCHAR(50) , region_1 VARCHAR(50) , country VARCHAR(20) , latitude FLOAT(20) , longitude FLOAT(20) );']

queries = remove_if_exist + create_tables

for query in queries:
    try:
        cursor.execute(query)
    except:
        print "Error in create: "+query
for i in range(1,1001):
    print(i)    
    id=str(i)
    try: 
        r = urllib.urlopen('http://sgp.postcodebase.com/node/'+ id).read()
    except IOError :
        continue
        
            
    soup = BeautifulSoup(r)
    
    
    try:
        e=soup.find_all("span", text="Title")[0]
        c=e.find_next_sibling("span")
        for child in c.children:
            b= child
        title=str(b.text)
    except IndexError:
        title = ""
    try:
        e=soup.find_all("span", text="Area 1")[0]
        c=e.find_next_sibling("span")
        for child in c.children:
            b= child
        area_1=str(b.text)
    except IndexError:
        area_1 = ""
    try:
        e=soup.find_all("span", text="Area 2")[0]
        c=e.find_next_sibling("span")
        for child in c.children:
            b= child
        area_2=str(b.text)
    except IndexError:
        area_2 = ""
    try:
        e=soup.find_all("span", text="Postcode (ZIP)")[0]
        c=e.find_next_sibling("span")
        for child in c.children:
            b= child
        postcode=int(b.text)
    except IndexError:
        postcode = ""
    try:
        e=soup.find_all("span", text="City")[0]
        c=e.find_next_sibling("span")
        for child in c.children:
            b= child
        city=str(b.text)
    except IndexError:
        city = ""
    try:
        e=soup.find_all("span", text="Region 3")[0]
        c=e.find_next_sibling("span")
        for child in c.children:
            b= child
        region_3=str(b.text)
    except IndexError:
        region_3 = ""
    
    try:
        e=soup.find_all("span", text="Region 2")[0]
        c=e.find_next_sibling("span")
        for child in c.children:
            b= child
        region_2=str(b.text)
    except IndexError:
        region_2 = ""
        
    try:
        e=soup.find_all("span", text="Region 1")[0]
        c=e.find_next_sibling("span")
        for child in c.children:
            b= child
        region_1=str(b.text)
    except IndexError:
        region_1 = ""
        
    try:
        e=soup.find_all("span", text="Country")[0]
        c=e.find_next_sibling("span")
        for child in c.children:
            b= child
        country = str(b.text)
    except IndexError:
        country = ""
    
    m=re.search('LatLng(\(.+?\))',soup.text)
    latlng = m.group(1)
    latlng = eval(latlng)    
    latitude= latlng[0]   
    longitude=latlng[1]
        
    try:
        cursor.execute("INSERT INTO PostcodeInformation VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(postcode,title,area_2,area_1,city,region_3,region_2,region_1,country,latitude,longitude))   
        con.commit()
    except:     
        pass
    
# -*- coding: utf-8 -*-
"""
Created on Thur Mar 31 10:44:32 2016

@author: Anshit
"""

from selenium import webdriver
import time
import lxml.html
import MySQLdb as mdb

con = mdb.connect('127.0.0.1', 'root', '', 'hdb')
cursor = con.cursor()

# Drop any existing tables and recreate them
# Creating a table that stores the city name as the primary key and weather condition in another columns
remove_if_exist = ['DROP TABLE IF EXISTS HDBRecord']
create_tables = ['CREATE TABLE HDBRecord (Block VARCHAR(25) , Street VARCHAR(50) , Storey VARCHAR(25) , Area VARCHAR(25) , Lease_date VARCHAR(25) , Price VARCHAR(25) , Resale_date VARCHAR(25));']


queries = remove_if_exist + create_tables

for query in queries:
	try:
		cursor.execute(query)
	except:
		print "Error in create: "+query


driver = webdriver.Firefox()

block=[]
street=[]
storey=[]
area=[]
date=[]
price=[]
resaledate=[]

url = "http://services2.hdb.gov.sg/webapp/BB33RTIS/BB33PReslTrans.jsp"

for i in range(1,8):     
	 for j in range(1,27):
		driver.get(url)
		time.sleep(3)
		el = driver.find_element_by_xpath('//*[@id="frmRTIS"]/div[1]/div/div[1]/div[2]/div[2]/select')
		option = el.find_elements_by_tag_name("option")
		el1 = driver.find_element_by_xpath('//*[@id="id_t"]')
		option1 = el1.find_elements_by_tag_name("option")
		option[i].click()
		option1[j].click()
		driver.find_element_by_xpath('//*[@id="dteRange"]').find_elements_by_tag_name("option")[2].click()
		driver.find_element_by_xpath('//*[@id="btnSubmit"]').click()
		time.sleep(3)
		hxs = lxml.html.document_fromstring(driver.page_source)
		try:
				table = hxs.xpath('//*[@id="divLargeDetail2"]/div/div[3]/table/tbody')[0]
				print len(table)
				for k in range(len(table)):
					block.append(hxs.xpath('//*[@id="divLargeDetail2"]/div/div[3]/table/tbody/tr['+str(k+1)+']/td[1]/a/text()')[0])
					street.append(hxs.xpath('//*[@id="divLargeDetail2"]/div/div[3]/table/tbody/tr['+str(k+1)+']/td[2]/text()')[0])
					storey.append(hxs.xpath('//*[@id="divLargeDetail2"]/div/div[3]/table/tbody/tr['+str(k+1)+']/td[3]/text()')[0])
					area.append(hxs.xpath('//*[@id="divLargeDetail2"]/div/div[3]/table/tbody/tr['+str(k+1)+']/td[4]/text()')[0])
					date.append(hxs.xpath('//*[@id="divLargeDetail2"]/div/div[3]/table/tbody/tr['+str(k+1)+']/td[5]/text()')[0])
					price.append(hxs.xpath('//*[@id="divLargeDetail2"]/div/div[3]/table/tbody/tr['+str(k+1)+']/td[6]/text()')[0])
					resaledate.append(hxs.xpath('//*[@id="divLargeDetail2"]/div/div[3]/table/tbody/tr['+str(k+1)+']/td[7]/text()')[0])

		except Exception as e:
			print e
			driver.back()                
			continue            
		   
		
		for k in range(len(table)):
			try:
				temp =  "INSERT INTO `HDBRecord`(`Block`, `Street`, `Storey`, `Area`, `Lease_date`, `Price`, `Resale_date`) VALUES ('%s','%s','%s','%s','%s','%s','%s');"%(block[k],street[k],storey[k],area[k],date[k],price[k],resaledate[k])
				print temp
				cursor.execute(temp)
				con.commit()
			except:
				con.rollback()

driver.close()
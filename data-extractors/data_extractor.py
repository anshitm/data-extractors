#
#
#This file contains source code for the data extractor

#Created : 4 April 2015
#Author: Anshit Mandloi
#		Ankit Deora
#
#


import sys 
import requests 
import lxml.html 
import MySQLdb as mdb
import time


# getMovies function returns a dictionary containing title,genre,year,rating and votes of a particular movie

def getMovie(id): 
	while(1):
		try:
			hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com/title/" + id).content)
			break
		except:
			time.sleep(.1)
			continue

	movie = {}
	try:
		movie['title'] = hxs.xpath('//*[@id="overview-top"]/h1/span[1]/text()')[0].strip()
	except IndexError:
		movie['title']
	try:
		movie['genre'] = hxs.xpath('//*[@id="overview-top"]/div[2]/a/span/text()')
	except IndexError:
		movie['genre'] = []
	try:
		movie['year'] = hxs.xpath('//*[@id="overview-top"]/h1/span[2]/a/text()')[0].strip()
	except IndexError:
		try:
			movie['year'] = hxs.xpath('//*[@id="overview-top"]/h1/span[3]/a/text()')[0].strip()
		except IndexError:
			movie['year'] = ""
	try:
		movie['rating'] = hxs.xpath('//*[@id="overview-top"]/div[3]/div[3]/strong/span/text()')[0].strip()
	except IndexError:
		movie['rating'] = ""
	try:
		movie['votes'] = hxs.xpath('//*[@id="overview-top"]/div[3]/div[3]/a[1]/span/text()')[0].strip()
	except IndexError:
		movie['votes'] = ""
	return movie

# can be made to read from input file ......I have given some prefix 
PREFIX = "divya"
url = "http://www.imdb.com/search/title?languages=hi%7C1&num_votes=50,&sort=user_rating,desc"


# connecting to the database and creating tables

db = mdb.connect('127.0.0.1', 'root', '', 'Divya');
cursor = db.cursor()
cursor.execute("drop table if exists "+PREFIX+"_M_Director;")
cursor.execute("drop table if exists "+PREFIX+"_M_Producer;")
cursor.execute("drop table if exists "+PREFIX+"_M_Cast;")
cursor.execute("drop table if exists "+PREFIX+"_M_Genre;")
cursor.execute("drop table if exists "+PREFIX+"_M_Language;")
cursor.execute("drop table if exists "+PREFIX+"_M_Country;")
cursor.execute("drop table if exists "+PREFIX+"_M_Location;")
cursor.execute("drop table if exists "+PREFIX+"_Movies;")
cursor.execute("drop table if exists "+PREFIX+"_Person;")
cursor.execute("drop table if exists "+PREFIX+"_Genre;")
cursor.execute("drop table if exists "+PREFIX+"_Language;")
cursor.execute("drop table if exists "+PREFIX+"_Country;")
cursor.execute("drop table if exists "+PREFIX+"_Location;")
sql = "create table "+PREFIX+"_Movies(MID varchar(11),title varchar(50),year integer,rating float,num_votes integer,constraint movie_pk primary key(MID));"
cursor.execute(sql)
cursor.execute("create table "+PREFIX+"_Person (PID varchar(11),Name varchar(30), DOB date, Gender varchar(7),constraint person_pk primary key(PID));")
cursor.execute("create table "+PREFIX+"_Genre (GID varchar(11),Name varchar(20),constraint genre_pk primary key(GID));")
cursor.execute("create table "+PREFIX+"_Language (LAID varchar(11),Name varchar(20),constraint language_pk primary key(LAID));")
cursor.execute("create table "+PREFIX+"_Country (CID varchar(11),Name varchar(20),constraint country_pk primary key(CID));")
cursor.execute("create table "+PREFIX+"_Location (LID varchar(11),Name varchar(50),constraint location_pk primary key(LID));")
cursor.execute("create table "+PREFIX+"_M_Producer (ID varchar(11),MID varchar(11),PID varchar(11),constraint m_pro_pk primary key(ID),constraint m_pro_fk foreign key(MID) references "+PREFIX+"_Movies (MID),constraint m_pro1_fk foreign key(PID) references "+PREFIX+"_Person(PID));")
cursor.execute("create table "+PREFIX+"_M_Director (ID varchar(11),MID varchar(11),PID varchar(11),constraint m_dir_pk primary key(ID),constraint m_dir_fk foreign key(MID) references "+PREFIX+"_Movies (MID),constraint m_dir1_fk foreign key(PID) references "+PREFIX+"_Person(PID));")
cursor.execute("create table "+PREFIX+"_M_Cast (ID varchar(11),MID varchar(11),PID varchar(11),constraint m_cast_pk primary key(ID),constraint m_cast_fk foreign key(MID) references "+PREFIX+"_Movies (MID),constraint m_cast1_fk foreign key(PID) references "+PREFIX+"_Person(PID));")
cursor.execute("create table "+PREFIX+"_M_Genre (ID varchar(11),MID varchar(11),GID varchar(11),constraint m_gen_pk primary key(ID),constraint m_gen_fk foreign key(MID) references "+PREFIX+"_Movies (MID),constraint m_gen1_fk foreign key(GID) references "+PREFIX+"_Genre(GID));")
cursor.execute("create table "+PREFIX+"_M_Language (ID varchar(11),MID varchar(11),LAID varchar(11),constraint m_lan_pk primary key(ID),constraint m_lan_fk foreign key(MID) references "+PREFIX+"_Movies (MID),constraint m_lan1_fk foreign key(LAID) references "+PREFIX+"_Language(LAID));")
cursor.execute("create table "+PREFIX+"_M_Country (ID varchar(11),MID varchar(11),CID varchar(11),constraint m_cou_pk primary key(ID),constraint m_cou_fk foreign key(MID) references "+PREFIX+"_Movies (MID),constraint m_cou1_fk foreign key(CID) references "+PREFIX+"_Country(CID));")
cursor.execute("create table "+PREFIX+"_M_Location (ID varchar(11),MID varchar(11),LID varchar(11),constraint m_loc_pk primary key(ID),constraint m_loc_fk foreign key(MID) references "+PREFIX+"_Movies (MID),constraint m_loc1_fk foreign key(LID) references "+PREFIX+"_Location(LID));")

# all the list is a dictionary which maps its key value to its id.

genre_list = {}
language_list = {}
country_list = {}
location_list = {}

# links containing all the id of movies ,this link might be like that of top 500 movies in bollywood 
url = url.strip()
links=[]
for i in range(1):                    # number of pages
	while(1):
		try:
			hxs= lxml.html.document_fromstring(requests.get(url+"&start="+str(i*50+1)+"&title_type=feature").content)
			break
		except:
			time.sleep(.1)
			continue
	titles=hxs.xpath('//*[@class="title"]')
	for title in titles:
		stri=title[0].attrib['data-tconst']
		links.append(stri)

# movies lists containing all the movies as a dictionary

movies = []
for i in range(3):                  # number of entries from each page
	movies.append(getMovie(links[i]))

for i in movies:
	i['votes'] = i['votes'].replace(',','')
	i['title'] = i['title'].replace('\'','')

# this loop contains all the details of producers, directors, casts and language

for i in range(len(movies)):
	while(1):
		try:
			hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com/title/"+links[i]+"/fullcredits?ref_=tt_ov_st_sm").content)
			break
		except:
			time.sleep(.1)
			continue
	dir_urls=hxs.xpath('//*[@id="fullcredits_content"]/table[1]/tbody/tr/td/a/@href')
	movies[i]['director'] = []
	for dir_url in dir_urls:
		dir_info = {}
		while(1):
			try:
				hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com"+dir_url).content)
				break
			except:
				time.sleep(.1)
				continue
		dir_info['id'] = dir_url.split('/')[2]
		try:
			dir_info['dob'] = hxs.xpath('//*[@id="name-born-info"]/time/@datetime')
		except IndexError:
			dir_info['dob'] = ""
		if len(dir_info['dob'])==0:
			dir_info['dob'] = ""
		else:
			dir_info['dob'] = dir_info['dob'][0].strip()
		try:
			dir_info['name'] = hxs.xpath('//*[@itemprop="name"]/text()')[0].strip() 
		except IndexError:
			dir_info['name'] = ""
		dir_info['name'] = dir_info['name'].replace('\'','')
		dir_info['gender'] = ""   # gender is not given
		movies[i]['director'].append(dir_info)

	while(1):
		try:
			hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com/title/"+links[i]+"/fullcredits?ref_=tt_ov_st_sm").content)
			break
		except:
			time.sleep(.1)
			continue
	cast_urls=hxs.xpath('//*[@itemprop="actor"]/a/@href')
	movies[i]['cast'] = []
	for cast_url in cast_urls:
		cast_info = {}
		while(1):
			try:
				hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com"+cast_url).content)
				break
			except:
				time.sleep(.1)
				continue
		cast_info['id'] = cast_url.split('/')[2]
		try:
			cast_info['dob'] = hxs.xpath('//*[@id="name-born-info"]/time/@datetime')
		except IndexError:
			cast_info['dob'] = ""
		if len(cast_info['dob'])==0:
			cast_info['dob'] = ""
		else:
			cast_info['dob'] = cast_info['dob'][0].strip()
		try:
			cast_info['name'] = hxs.xpath('//*[@itemprop="name"]/text()')[0].strip() 
		except IndexError:
			cast_info['name'] = ""
		cast_info['name'] = cast_info['name'].replace('\'','')
		try:
			cast_info['gender'] = hxs.xpath('//*[@id="name-job-categories"]/a[1]/span/text()')[0].strip() 
		except IndexError:
			cast_info['gender'] = ""
		try:
			cast_info['gender'] = hxs.xpath('//*[@id="name-job-categories"]/a[1]/span/text()')[0].strip() 
		except IndexError:
			cast_info['gender'] = ""
		if(cast_info['gender'] == "Actor"):
			cast_info['gender'] = "male"
		elif(cast_info['gender'] == "Actress"):
			cast_info['gender'] = "female"
		else:
			cast_info['gender'] = ""
		movies[i]['cast'].append(cast_info)

	while(1):
		try:
			hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com/title/"+links[i]+"/fullcredits?ref_=tt_ov_st_sm").content)
			break
		except:
			time.sleep(.1)
			continue
	pro_urls=hxs.xpath('//*[@id="fullcredits_content"]/table[4]/tbody/tr/td/a/@href')
	movies[i]['producer'] = []
	for pro_url in pro_urls:
		pro_info = {}
		while(1):
			try:
				hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com"+pro_url).content)
				break
			except:
				time.sleep(.1)
				continue
		pro_info['id'] = pro_url.split('/')[2]
		try:
			pro_info['dob'] = hxs.xpath('//*[@id="name-born-info"]/time/@datetime')
		except IndexError:
			pro_info['dob'] = ""
		if len(pro_info['dob'])==0:
			pro_info['dob'] = ""
		else:
			pro_info['dob'] = pro_info['dob'][0].strip()
		try:
			pro_info['name'] = hxs.xpath('//*[@itemprop="name"]/text()')[0].strip() 
		except IndexError:
			pro_info['name'] = ""
		pro_info['name'] = pro_info['name'].replace('\'','')
		pro_info['gender'] = ""
		movies[i]['producer'].append(pro_info)

	while(1):
		try:
			hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com/title/"+links[i]).content)
			break
		except:
			time.sleep(.1)
			continue
	movies[i]['language'] = hxs.xpath('//*[@id="titleDetails"]/div[contains(h4/text(),"Language:")]/a/text()')
	for j in range(len(movies[i]['language'])):
		movies[i]['language'][j] = movies[i]['language'][j].strip()
	movies[i]['country'] = hxs.xpath('//*[@id="titleDetails"]/div[contains(h4/text(),"Country:")]/a/text()')
	for j in range(len(movies[i]['country'])):
		movies[i]['country'][j] = movies[i]['country'][j].strip()

 # filling databases
id_dir = 0
id_cast = 0
id_pro = 0
for i in range(len(movies)):
	try:
		cursor.execute("insert into "+PREFIX+"_Movies VALUES (%s,%s,%s,%s,%s)",(links[i],movies[i]['title'],int(movies[i]['year']),float(movies[i]['rating']),int(movies[i]['votes'])))
		db.commit()
	except:
		db.rollback()
	for j in range(len(movies[i]['producer'])):
		try:
			cursor.execute("insert into "+PREFIX+"_Person VALUES (%s,%s,%s,%s)",(movies[i]['producer'][j]['id'],movies[i]['producer'][j]['name'],movies[i]['producer'][j]['dob'],movies[i]['producer'][j]['gender']))
			db.commit()
		except:
			db.rollback()
		try:
			cursor.execute("insert into "+PREFIX+"_M_Producer VALUES (%s,%s,%s)",("mpro"+str(id_pro),links[i],movies[i]['producer'][j]['id']))
			db.commit()
		except:
			db.rollback()
		id_pro += 1
	for j in range(len(movies[i]['cast'])):
		try:
			cursor.execute("insert into "+PREFIX+"_Person VALUES (%s,%s,%s,%s)",(movies[i]['cast'][j]['id'],movies[i]['cast'][j]['name'],movies[i]['cast'][j]['dob'],movies[i]['cast'][j]['gender']))
			db.commit()
		except:
			db.rollback()
		try:
			cursor.execute("insert into "+PREFIX+"_M_Cast VALUES (%s,%s,%s)",("mcast"+str(id_cast),links[i],movies[i]['cast'][j]['id']))
			db.commit()
		except:
			db.rollback()
		id_cast += 1
	for j in range(len(movies[i]['director'])):
		try:
			cursor.execute("insert into "+PREFIX+"_Person VALUES (%s,%s,%s,%s)",(movies[i]['director'][j]['id'],movies[i]['director'][j]['name'],movies[i]['director'][j]['dob'],movies[i]['director'][j]['gender']))
			db.commit()
		except:
			db.rollback()
		try:
			cursor.execute("insert into "+PREFIX+"_M_Director VALUES (%s,%s,%s)",("mdir"+str(id_dir),links[i],movies[i]['director'][j]['id']))
			db.commit()
		except:
			db.rollback()
		id_dir += 1

# extracting genre data
while(1):
	try:
		hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com/genre/").content)
		break
	except:
		time.sleep(.1)
		continue
gen_names = hxs.xpath('//*[@id="main"]/div[1]/table/tr/td/h3/a/text()')
gen_urls=hxs.xpath('//*[@id="main"]/div[1]/table/tr/td/h3/a/@href')
for i in range(len(gen_names)):
	gen_names[i]=gen_names[i].strip()
tot_genre = []
for i in range(len(gen_urls)):
	genre = {}
	genre['name'] = gen_names[i]
	genre['gid'] = gen_urls[i].split('/')[5].split("gnr_")[1]
	tot_genre.append(genre)

# filling genre database
for i in range(len(tot_genre)):
	genre_list[tot_genre[i]['name']] = tot_genre[i]['gid']
	try:
		cursor.execute("insert into "+PREFIX+"_Genre VALUES (%s,%s)",(tot_genre[i]['gid'],tot_genre[i]['name']))
		db.commit()
	except:
		db.rollback()

# extracting language database
while(1):
	try:
		hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com/language/").content)
		break
	except:
		time.sleep(.1)
		continue
lan_ids=hxs.xpath('//*[@id="main"]/table[1]/tr/td/a/@href')
lan_names=hxs.xpath('//*[@id="main"]/table[1]/tr/td/a/text()')
for i in range(len(lan_ids)):
	lan_ids[i] = lan_ids[i].split('/')[2]

# filling language database
for i in range(len(lan_ids)):
	language_list[lan_names[i]] = lan_ids[i]
	try:
		cursor.execute("insert into "+PREFIX+"_Language VALUES (%s,%s)",(lan_ids[i],lan_names[i]))
		db.commit()
	except:
		db.rollback()

# extracting country database
while(1):
	try:
		hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com/country/").content)
		break
	except:
		time.sleep(.1)
		continue
cou_ids=hxs.xpath('//*[@id="main"]/table[1]/tr/td/a/@href')
cou_names=hxs.xpath('//*[@id="main"]/table[1]/tr/td/a/text()')
for i in range(len(cou_ids)):
	cou_ids[i] = cou_ids[i].split('/')[2]
	cou_names[i] = cou_names[i].strip()

# filling country database
for i in range(len(cou_ids)):
	country_list[cou_names[i]] = cou_ids[i]
	try:
		cursor.execute("insert into "+PREFIX+"_Country VALUES (%s,%s)",(cou_ids[i],cou_names[i]))
		db.commit()
	except:
		db.rollback()

# extracting location datas
loc_id = 0
for i in range(len(movies)):
	while(1):
		try:
			hxs= lxml.html.document_fromstring(requests.get("http://www.imdb.com/title/"+links[i]+"/locations?ref_=tt_ql_dt_6").content)
			break
		except:
			time.sleep(.1)
			continue
	locations = hxs.xpath('//*[@id="filming_locations_content"]/div/dt/a/text()')
	for j in range(len(locations)):
		locations[j] = locations[j].strip()
		if~(locations[j] in location_list):
			location_list[locations[j]] = loc_id
			loc_id += 1
	movies[i]['location'] = locations

# filling location,M_location,m_genre,m_language,m_country databases
id_genre = 0
id_lang = 0
id_coun = 0
id_loc = 0
for i in range(len(movies)):
	for j in location_list:
		try:
			cursor.execute("insert into "+PREFIX+"_Location VALUES (%s,%s)",(location_list[j],j))
			db.commit()
		except:
			db.rollback()
	for j in range(len(movies[i]['location'])):
		try:
			cursor.execute("insert into "+PREFIX+"_M_Location VALUES (%s,%s,%s)",("mloc"+str(id_loc),links[i],location_list[movies[i]['location'][j]]))
			db.commit()
		except:
			db.rollback()
		id_loc += 1

	for j in range(len(movies[i]['genre'])):
		try:
			cursor.execute("insert into "+PREFIX+"_M_Genre VALUES (%s,%s,%s)",("mgen"+str(id_genre),links[i],genre_list[movies[i]['genre'][j]]))
			db.commit()
		except:
			db.rollback()
		id_genre += 1
	for j in range(len(movies[i]['language'])):
		try:
			cursor.execute("insert into "+PREFIX+"_M_Language VALUES (%s,%s,%s)",("mlang"+str(id_lang),links[i],language_list[movies[i]['language'][j]]))
			db.commit()
		except:
			db.rollback()
		id_lang += 1
	for j in range(len(movies[i]['country'])):
		try:
			cursor.execute("insert into "+PREFIX+"_M_Country VALUES (%s,%s,%s)",("mcoun"+str(id_coun),links[i],country_list[movies[i]['country'][j]]))
			db.commit()
		except:
			db.rollback()
		id_coun += 1
